<?php
if (API_INITIALIZED != "true") exit;

$blnError = false;

$strFullName         = filter_var($vars->full_name, FILTER_SANITIZE_STRING);
$strPhone            = $vars->phone_no; //filter_var($vars->phone_no, FILTER_SANITIZE_STRING);
$strAddress          = filter_var($vars->address, FILTER_SANITIZE_STRING);
$strGender           = filter_var($vars->gender, FILTER_SANITIZE_STRING);
$strAge              = $vars->age; //filter_var($vars->age, FILTER_SANITIZE_STRING);
$strAddressLat       = $vars->address_lat;
$strAddressLong      = $vars->address_long;
$strMemberId         = null;
$arrResult           = array();

// Check empty fields
if ($strFullName == "" or 
        $strPhone == "" or
        $strAddress == "" or
        $strAge == "" or 
        $strGender == "") {
    $blnError = true;
    $strErrorMessage = "Kolom pengisian tidak lengkap";
} 

if (!$strAddressLat){
    $strAddressLat = 0;
}

if (!$strAddressLong){
    $strAddressLong = 0;
}

if (!$blnError) {
    $strSQL = "SELECT * FROM user WHERE phone = '$strPhone'";
    $arrResult["row"] = $db->get_row($strSQL);
    if (!$arrResult["row"]){
        $strSQL = "INSERT INTO user (
            full_name, 
            age,
            phone,
            gender,
            location,
            last_lat,
            last_long,
            active,
            last_update
            ) VALUES (
            '{$strFullName}',
            '{$strAge}',
            '{$strPhone}',
            '{$strGender}',
            '{$strAddress}',
            {$strAddressLat},
            {$strAddressLong},
            '1',
            '" . date("Y-m-d H:i:s") . "'
            )";
        $blnSuccess = $db->query($strSQL);

        if (!$blnSuccess) {
            $blnError = true;
            $strErrorMessage = "Database error";
        } else {
            $strMemberId  = $db->last_insert_id();

            $strSQL = "SELECT id, full_name, location, phone, last_lat, last_long FROM user WHERE id = '$strMemberId'";
            $arrResult["row"] = $db->get_row($strSQL);

            $strErrorMessage = "Registrasi Anda berhasil. Silakan update kesehatan Anda";
        }
    } else {
        $strSQL = "UPDATE user SET 
                    full_name = '{$strFullName}', 
                    age = '{$strAge}', 
                    gender = '{$strGender}', 
                    location = '{$strAddress}', 
                    last_lat = '{$strAddressLat}', 
                    last_long = '{$strAddressLong}', 
                    active = '1',
                    last_update = '" . date("Y-m-d H:i:s") . "'
                WHERE phone = '{$strPhone}'";
        $db->query($strSQL);

        $strSQL = "SELECT id, full_name, location, phone, last_lat, last_long FROM user WHERE phone = '$strPhone'";
        $arrResult["row"] = $db->get_row($strSQL);

        $strSQL = "SELECT status, update_date FROM status_update WHERE phone_no = '$strPhone'";
        $strUpdated = $db->get_row($strSQL);
        if ($strUpdated){
            $arrResult["row"]["last_update"] = $strUpdated["update_date"];
            $arrResult["row"]["last_status"] = $strUpdated["status"];
        }
        $strErrorMessage = "Selamat datang kembali, {$arrResult["row"]["full_name"]}";
    }
}

if (!$blnError) {
    $arrResult["request"]["status"] = "success";
    $arrResult["request"]["message"] = $strErrorMessage;
    // $arrResult["request"]["user_id"] = $strMemberId;
} else {
    $arrResult["request"]["status"] = "failed";
    $arrResult["request"]["message"] = $strErrorMessage;
}

echo json_encode($arrResult);
exit;
?>