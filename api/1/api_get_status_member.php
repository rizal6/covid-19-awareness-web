<?php
if (API_INITIALIZED != "true") exit;

$blnError = false; 

$strUserId         = filter_var($vars->user_id, FILTER_SANITIZE_STRING);
$arrUpdate = null;
$arrUser   = null;

if ($strUserId == "") {
    $blnError = true;
    $strErrorMessage = "Kolom pengisian tidak lengkap";
}

if (!$blnError) {
    $strSQL = "SELECT * FROM user WHERE id = '{$strUserId}'";
    $arrUser = $db->get_row($strSQL);
    if ($arrUser){
        $arrUser["when"] = timeSince($arrUser["last_update"]);
        $strSQL = "SELECT * FROM status_update WHERE phone_no = '{$arrUser["phone"]}' ORDER BY update_date DESC LIMIT 1";
        $arrUpdate = $db->get_row($strSQL);
        if ($arrUpdate){
            $arrUpdate["when"] = timeSince($arrUpdate["update_date"]);
        }
    }
} 

$arrResult = array();

if (!$blnError) {
    $arrResult["request"]["status"] = "success";
    $arrResult["request"]["user"] = $arrUser;
    $arrResult["request"]["status_update"] = $arrUpdate;
} else {
    $arrResult["request"]["status"] = "failed";
    $arrResult["request"]["message"] = $strErrorMessage;
}

echo json_encode($arrResult);

exit;
?>