<?php
if (API_INITIALIZED != "true") exit;

$blnError = false;
$arrResult = array();

if (!$blnError) {
    $strSQL = "SELECT * FROM user";
    $arrResult["row"] = $db->get_results($strSQL);
    if (isset($arrResult["row"])){
        foreach ($arrResult["row"] as $i => $usr) {
            # code...
            $strSQL = "SELECT * FROM status_update WHERE phone_no = '{$arrResult["row"][$i]["phone"]}' ORDER BY update_date DESC LIMIT 1";
            $arrUpdate = $db->get_row($strSQL);
            if (isset($arrUpdate)){
                $arrResult["row"][$i]["last_status"] = $arrUpdate["status"];
                $arrResult["row"][$i]["last_latitude"] = $arrUpdate["latitude"];
                $arrResult["row"][$i]["last_longitude"] = $arrUpdate["longitude"];
                $arrResult["row"][$i]["last_update_date"] = $arrUpdate["update_date"];
                $arrResult["row"][$i]["last_when"] = timeSince($arrUpdate["update_date"]);
            } else {
                $arrResult["row"][$i]["last_status"] = $arrResult["row"][$i]["last_status"];
                $arrResult["row"][$i]["last_latitude"] = $arrResult["row"][$i]["last_lat"];
                $arrResult["row"][$i]["last_longitude"] = $arrResult["row"][$i]["last_long"];
                $arrResult["row"][$i]["last_update_date"] = $arrResult["row"][$i]["last_update"];
                $arrResult["row"][$i]["last_when"] = timeSince($arrResult["row"][$i]["last_update"]);
            }
        }
    } 
}

if (!$blnError) {
    $arrResult["request"]["status"] = "success";
} else {
    $arrResult["request"]["status"] = "failed";
    $arrResult["request"]["message"] = $strErrorMessage;
}

echo json_encode($arrResult);
exit;
?>