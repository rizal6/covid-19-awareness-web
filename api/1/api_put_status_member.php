<?php
if (API_INITIALIZED != "true") exit;

$blnError = false;

$strUserId         = $vars->userId;
$strPhone          = $vars->userPhone; 
$checkedCough       = $vars->checkedCough;
$checkedSneeze      = $vars->checkedSneeze;
$checkedTired       = $vars->checkedTired;
$checkedFever       = $vars->checkedFever;
$checkedBreath      = $vars->checkedBreath;
$addressLatitude      = $vars->address_lat;
$addressLongitude      = $vars->address_long;

$statusUser = null;

// Check empty fields
if ($strUserId == "" or 
        $strPhone == "") {
    $blnError = true;
    $strErrorMessage = "Kolom pengisian tidak lengkap";
} 

if (!$blnError) {
    $strSQL = "SELECT * FROM user WHERE id = '$strUserId'";
    $arrMember = $db->get_row($strSQL);
    if ($db->num_rows() > 0) {

        if ($checkedCough == true AND 
                $checkedSneeze == true AND 
                $checkedTired == true AND 
                $checkedFever == true AND
                $checkedBreath == true) {
            $statusUser = "suspect";
        } else if ($checkedCough == true AND 
                $checkedSneeze == true AND 
                $checkedTired == false AND 
                $checkedFever == true AND
                $checkedBreath == false OR 
                                    $checkedCough == true OR
                                    $checkedSneeze == true OR
                                    $checkedTired == true OR
                                    $checkedFever == true OR
                                    $checkedBreath == true) {
            $statusUser = "sick";
        } else {
            $statusUser = "normal";
        }

        if ($checkedCough){
            $checkedCough = 1;
        } else {
            $checkedCough = 0;
        }

        if ($checkedSneeze){
            $checkedSneeze = 1;
        } else {
            $checkedSneeze = 0;
        }

        if ($checkedTired){
            $checkedTired = 1;
        } else {
            $checkedTired = 0;
        }

        if ($checkedFever){
            $checkedFever = 1;
        } else {
            $checkedFever = 0;
        }

        if ($checkedBreath){
            $checkedBreath = 1;
        } else {
            $checkedBreath = 0;
        }

        $checklist = $checkedCough.",".$checkedSneeze.",".$checkedTired.",".$checkedFever.",".$checkedBreath;

        $strSQL = "INSERT INTO status_update (
            phone_no, 
            checklist,
            latitude,
            longitude,
            status,
            update_date
            ) VALUES (
            '{$strPhone}',
            '{$checklist}',
            '{$addressLatitude}',
            '{$addressLongitude}',
            '{$statusUser}',
            '" . date("Y-m-d H:i:s") . "'
        )";
        $blnSuccess = $db->query($strSQL);

        if (!$blnSuccess) {
            $blnError = true;
            $strErrorMessage = "Database error";
        } 
    } else {
        $blnError = true;
        $strErrorMessage = "User Tidak Ditemukan";
    }
}

$arrResult = array();

if (!$blnError) {
    $arrResult["request"]["status"] = "success";
    $arrResult["request"]["update_status"] = $statusUser;
} else {
    $arrResult["request"]["status"] = "failed";
    $arrResult["request"]["message"] = $strErrorMessage;
}

echo json_encode($arrResult);

exit;
?>