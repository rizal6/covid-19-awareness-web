var LIBS = {
	// Chart libraries
	plot: [
		"assets/lib/flot/jquery.flot.min.js",
		"assets/lib/flot/jquery.flot.pie.min.js",
		"assets/lib/flot/jquery.flot.stack.min.js",
		"assets/lib/flot/jquery.flot.resize.min.js",
		"assets/lib/flot/jquery.flot.curvedLines.js",
		"assets/lib/flot/jquery.flot.tooltip.min.js",
		"assets/lib/flot/jquery.flot.categories.min.js"
	],
	chart: [
		'assets/lib/echarts/build/dist/echarts-all.js',
		'assets/lib/echarts/build/dist/theme.js',
		'assets/lib/echarts/build/dist/jquery.echarts.js'
	],
	circleProgress: [
		"assets/lib/jquery-circle-progress/dist/circle-progress.js"
	],
	sparkline: [
		"assets/lib/sparkline/jquery.sparkline.min.js"
	],
	maxlength: [
		"assets/lib/bootstrap-maxlength/src/bootstrap-maxlength.js"
	],
	tagsinput: [
		"assets/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput.css",
		"assets/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js",
	],
	TouchSpin: [
		"assets/lib/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css",
		"assets/lib/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"
	],
	selectpicker: [
		"assets/lib/bootstrap-select/dist/css/bootstrap-select.min.css",
		"assets/lib/bootstrap-select/dist/js/bootstrap-select.min.js"
	],
	filestyle: [
		"assets/lib/bootstrap-filestyle/src/bootstrap-filestyle.min.js"
	],
	timepicker: [
		"assets/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"
	],
	datetimepicker: [
		"assets/lib/moment/moment.js",
		"assets/lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
		"assets/lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"
	],
	select2: [
		"assets/lib/select2/dist/css/select2.min.css",
		"assets/lib/select2/dist/js/select2.full.min.js"
	],
	vectorMap: [
		"assets/lib/jvectormap/jquery-jvectormap.css",
		"assets/lib/jvectormap/jquery-jvectormap.min.js",
		"assets/lib/jvectormap/maps/jquery-jvectormap-us-mill.js",
		"assets/lib/jvectormap/maps/jquery-jvectormap-world-mill.js",
		"assets/lib/jvectormap/maps/jquery-jvectormap-africa-mill.js"
	],
	summernote: [
		"assets/lib/summernote/dist/summernote.css",
		"assets/lib/summernote/dist/summernote.min.js"
	],
	DataTable: [
		"assets/lib/datatables/datatables.min.css",
		"assets/lib/datatables/datatables.min.js"
	],
	fullCalendar: [
		"assets/lib/moment/moment.js",
		"assets/lib/fullcalendar/dist/fullcalendar.min.css",
		"assets/lib/fullcalendar/dist/fullcalendar.min.js"
	],
	dropzone: [
		"assets/lib/dropzone/dist/min/dropzone.min.css",
		"assets/lib/dropzone/dist/min/dropzone.min.js"
	],
	counterUp: [
		"assets/lib/waypoints/lib/jquery.waypoints.min.js",
		"assets/lib/counterup/jquery.counterup.min.js"
	],
	others: [
		"assets/lib/switchery/dist/switchery.min.css",
		"assets/lib/switchery/dist/switchery.min.js",
		"assets/lib/lightbox2/dist/css/lightbox.min.css",
		"assets/lib/lightbox2/dist/js/lightbox.min.js"
	]
};