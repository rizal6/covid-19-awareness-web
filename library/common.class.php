<?php
function base64ToJpeg($base64_string, $output_file) {
    $ifp = fopen($output_file, "wb"); 

    fwrite($ifp, base64_decode($base64_string)); 
    fclose($ifp); 

    return $output_file; 
}

function timeSince($strDate) {
    $since = time() - strtotime($strDate);

    $chunks = array(
        array(60 * 60 * 24 * 365 , 'tahun'),
        array(60 * 60 * 24 * 30 , 'bulan'),
        array(60 * 60 * 24 * 7, 'minggu'),
        array(60 * 60 * 24 , 'hari'),
        array(60 * 60 , 'jam'),
        array(60 , 'menit'),
        array(1 , 'detik')
    );

    for ($i = 0, $j = count($chunks); $i < $j; $i++) {
        $seconds = $chunks[$i][0];
        $name = $chunks[$i][1];
        if (($count = floor($since / $seconds)) != 0) {
            break;
        }
    }

    $print = ($count == 1) ? '1 '.$name : "$count {$name}";
    return $print;
}
?>