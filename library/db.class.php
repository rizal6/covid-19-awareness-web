<?php

class db {

	private $connection;

	private $hostName;

	private $userName;

	private $password;

	private $database;

	private $arrResult;

	private $display_error;

	public $error_message;

	private $last_insert_id;

	private $num_rows;



	/**

	 * DB Constructor

	 *

	 * @param varchar DatabaseHostName

	 * @param varchar DatabaseName

	 * @param varchar DatabaseUserName

	 * @param varchar DatabasePassword

	 * @return db

	 */

	function __construct($strDBHostName, $strDBName, $strDBUserName, $strDBPassword) {

		$this->hostName      = $strDBHostName;

		$this->userName      = $strDBUserName;

		$this->password      = $strDBPassword;

		$this->database      = $strDBName;

		$this->display_error = false;

		$this->arrResult     = array();

		$this->connect();

	}



	function connect() {

	    $this->connection = mysqli_connect($this->hostName, $this->userName, $this->password) or die(mysqli_error($this->connection));

	    mysqli_select_db($this->connection, $this->database);

        $x=mysqli_error($this->connection);

	}



	/**

	 * Run query and return true or false

	 *

	 * @param string $strSQL

	 * @return boolean true/false

	 */

	function query($strSQL) {

	    $this->connect();

		$rstSQL = mysqli_query($this->connection, $strSQL);

        $x=mysqli_error($this->connection);

		if (preg_match("/^insert*/i",$strSQL)) {

		    $this->last_insert_id = mysqli_insert_id($this->connection);

		}



		if ($rstSQL) {

		    mysqli_close($this->connection);

			return true;

		} else {

			if ($this->display_error == true) echo mysqli_error($this->connection);
			
			$this->error_message = mysqli_error($this->connection);

		    mysqli_close($this->connection);

			return false;

		}

	}



	/**

	 * Get all table records from query

	 *

	 * @param string $strSQL

	 * @return array

	 */

	function get_results($strSQL) {

		$this->connect();

	    $this->arrResult = array();

		$rstSQL          = mysqli_query($this->connection, $strSQL);

		if (!$rstSQL) {

		    if ($this->display_error == true) echo mysqli_error($this->connection);

		    mysqli_close($this->connection);

		    return null;

		}



		mysqli_close($this->connection);



		$intSQLFields   = mysqli_num_fields($rstSQL);

		$intSQLRows     = mysqli_num_rows($rstSQL);

		$this->num_rows = $intSQLRows;



		if ($intSQLRows > 0) {

		    $intRecordRow = 0;

		    while ($rowSQL = mysqli_fetch_array($rstSQL, MYSQLI_ASSOC)) {

			    for ($i = 0; $i < $intSQLFields; $i++) {

			 	    //$strFieldName  = $this->mysqli_field_name($rstSQL, $i);

			  	    //$strFieldValue = $rowSQL[$this->mysqli_field_name($rstSQL, $i)];

			        $this->arrResult[$intRecordRow] = $rowSQL;

    		    }

    		   $intRecordRow++;

		    }

		    return $this->arrResult;

		} else {

			return null;

		}

	}



	/**

	 * Get 1 row table records from query

	 *

	 * @param string $strSQL

	 * @return array

	 */

	function get_row($strSQL) {

	    $this->connect();

	    $this->arrResult = array();

		$rstSQL = mysqli_query($this->connection, $strSQL);



		if (!$rstSQL) {

		    if ($this->display_error = true) echo mysqli_error($this->connection);

		    mysqli_close($this->connection);

		    return null;

		}



		mysqli_close($this->connection);



		$intSQLFields   = mysqli_num_fields($rstSQL);

		$intSQLRows     = mysqli_num_rows($rstSQL);

		$this->num_rows = $intSQLRows;

		if ($intSQLRows > 0) {

			$rowSQL = mysqli_fetch_assoc($rstSQL);

            while ($fieldInfo = mysqli_fetch_field($rstSQL)) {
                $this->arrResult[$fieldInfo->name] = $rowSQL[$fieldInfo->name];
			}
			
		    return $this->arrResult;
			exit;
		} else {

			return null;

		}

	}



	/**

	 * Get field name from query

	 *

	 * @param string $strSQL

	 * @return array

	 */

	function get_fields($strSQL) {

	    $this->connect();

	    $this->arrResult = "";

		$rstSQL = mysqli_query($this->connection, $strSQL);



		if (!$rstSQL) {

		    if ($this->display_error == true) echo mysqli_error($this->connection);

		     mysqli_close($this->connection);

		    return null;

		}



		mysqli_close($this->connection);



		$intSQLFields = mysqli_num_fields($rstSQL);

		$intSQLRows   = mysqli_num_rows($rstSQL);

		#if ($intSQLRows > 0) {

            for ($intFieldRow = 0; $intFieldRow < $intSQLFields; $intFieldRow++) {

    	        $this->arrResult[$intFieldRow] = $this->mysqli_field_name($rstSQL, $intFieldRow);

            }

		    return $this->arrResult;

		#} else {

		#	return null;

		#}

	}



	/**

	 * Get SQL variable from query

	 *

	 * @param string $strSQL

	 * @param string total_records|total_fields

	 * @return string

	 */

	function get_var($strSQL, $strType = "") {

	    $this->connect();

	    $this->arrResult = "";

		$rstSQL	= mysqli_query($this->connection, $strSQL);



		if (!$rstSQL) {

		    if ($this->display_error = true) echo mysqli_error($this->connection);

		    mysqli_close($this->connection);

		    return null;

		}



		mysqli_close($this->connection);



		switch ($strType) {

			case "total_records":

				return mysqli_num_rows($rstSQL);

				break;



			case "total_fields":

				return mysqli_num_fields($rstSQL);

				break;



			case "":

			    $rowSQL = mysqli_fetch_row($rstSQL);

                return($rowSQL[0]);

			    break;

		}

	}



	/**

	 * Return number of records

	 *

	 * @param varchar $strSQL

	 * @return number

	 */

	function num_rows($strSQL = "") {

	    if ($strSQL) {

	        $this->connect();

	        $rstSQL	= mysqli_query($this->connection, $strSQL);



	        if (!$rstSQL) {

	            if ($this->display_error == true) echo mysqli_error($this->connection);

	            mysqli_close($this->connection);

	            return null;

	        }



	        mysqli_close($this->connection);

	        return mysqli_num_rows($rstSQL);

	    } else {

           return $this->num_rows;

	    }

	}



	/**

	 * Hide php error by altering php configuration

	 *

	 */

	function hide_errors() {

	    $this->display_error = false;

	    ini_set("error_reporting", "~E_ALL | ~E_STRICT | ~E_NOTICE");

	}



	/**

	 * Return last auto generated ID

	 *

	 * @return int

	 */

	function last_insert_id() {

	    return $this->last_insert_id;

	}
    
    function mysqli_field_name($result, $field_offset) {
        $properties = mysqli_fetch_field_direct($result, $field_offset);
        return is_object($properties) ? $properties->name : false;
	}
	

	/**
	 * 
	 * API GET PROVINCE INDONESIA
	 * 
	 */

}

?>