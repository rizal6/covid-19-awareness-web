<?php
ini_set("allow_url_fopen", true);
ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);

define("BOOTSTRAP", "true");

include("config.php");
include("library/db.class.php");
// include("library/email.class.php");
include('library/common.class.php');
// include('library/location.class.php');

$db = new db(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

$post = file_get_contents("php://input");
$vars = json_decode($post);
if (!$vars or !$post) {
    error_log($vars, 0);
    error_log("php://input is blank", 0);
    $arrResult = array();
    $arrResult["request"]["status"] = "failed";
    $arrResult["request"]["message"] = "Request is blank";
    echo json_encode($arrResult);
}

$apiFolder = "1";
if ($vars->appType == "user") {
    if ($vars->appVersionCode){
        if ($vars->appVersionCode >= 1){
            $apiFolder = "1";
        }
    }
} 

if ($vars->act == "RegisterMember") {
    include("api/".$apiFolder."/api_register_member.php");
}

if ($vars->act == "GetStatusUser") {
    include("api/".$apiFolder."/api_get_status_member.php");
}

if ($vars->act == "PutStatusUser") {
    include("api/".$apiFolder."/api_put_status_member.php");
}

if ($vars->act == "GetStatusUserAll") {
    include("api/".$apiFolder."/api_put_status_member_all.php");
}

?>