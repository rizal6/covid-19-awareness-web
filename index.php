<?php 
ini_set('session.gc_maxlifetime', 3600);
session_set_cookie_params(3600);

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
session_start();

define("BOOTSTRAP", "true");

include("config.php");
include("library/db.class.php");
include("library/email.class.php");
include('library/PushBots.class.php');
include('library/common.class.php');
include('library/slack.class.php');

$db = new db(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);

$vars = $_REQUEST;

if (!$vars["mod"]) $vars["mod"] = "login";

if ($vars["mod"]) {
    if ($vars["mod"] != "login" and $vars["mod"] != "activation" and $vars["mod"] != "reset_password") {
        if ($_SESSION["user_id"]) {
            include("controller/{$vars["mod"]}.php");
        } else {
            $_SESSION['last_page'] =  BASE_URL . $_SERVER['REQUEST_URI'];
            header("location: " . BASE_URL . BASE_PATH . "?mod=login");
            exit;
        }
    } else {   
        include("controller/{$vars["mod"]}.php");
    }
} else {
    die("Page not found");
}
?>